﻿# ![CaseTalk Logo](https://www.casetalk.com/images/icons/casetalk.png) DCA core model.prj
*Project created with CaseTalk v12.2 Build 1.28365.*

* ![Project](https://www.casetalk.com/images/icons/prj.png) DCA core model.prj
  * ![Repository](https://www.casetalk.com/images/icons/ig.png) dca core model.ig
    * ![Expression File](https://www.casetalk.com/images/icons/exp.png) dca core-concepts.exp
    * ![Diagram](https://www.casetalk.com/images/icons/igd.png) dca core-concepts.igd
    * ![Repository](https://www.casetalk.com/images/icons/igg.png) dca core model_GLR.igg
      * ![Basic File](https://www.casetalk.com/images/icons/txt.png) dca core model_GLR_ERStudio.bas
## dca core-concepts.igd
![Diagram dca core-concepts.igd](dca core-concepts.png)
