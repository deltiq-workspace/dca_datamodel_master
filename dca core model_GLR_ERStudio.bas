'----------
'- CASETALK GENERATED VBSCRIPT
'- PLEASE RUN INSIDE ERSTUDIO USING TOOLS\BASIC MACRO EDITOR
'----------

'----------
'- Project=DCA core model.prj
'- Path=C:\Users\wouter.van.aerle\Documents\dca_datamodel_master\
'- Description=
'- Repository=C:\Users\wouter.van.aerle\Documents\dca_datamodel_master\dca core model_GLR.igg
'- Author=Wouter van Aerle/wouter.van.aerle@deltiq.com
'----------

Sub Main
DiagramManager.EnableScreenUpdateEx(False, True) ' Turn off screen updates

' -----------------------------------------
' Configuration parameters for this script:
Set forceNew = False
Set createAttributes = True
Set createViews = True
' -----------------------------------------

Set MyDiagram = DiagramManager.DiagramItem("DCA core model")
if (forceNew) or (MyDiagram is Nothing) then
    Set MyDiagram = DiagramManager.NewDiagram
    end if
    MyDiagram.ProjectName = "DCA core model"
    MyDiagram.Author = "Wouter van Aerle/wouter.van.aerle@deltiq.com"
    MyDiagram.Description = ""
Set MyDictionary = MyDiagram.Dictionary
Set MyModel = MyDiagram.Models.Item("dca core model_GLR")
if MyModel is Nothing then
Set MyModel = MyDiagram.ActiveModel
' or .. Set MyModel = MyDiagram.Models.Add("dca core model_GLR", 1) ' 1 = SQL Server
    MyModel.Name = "dca core model_GLR"
    MyModel.SpaceHandling = 0 ' Handling of spaces in role names
    end if 

Dim att As AttachmentType
' Create model attachments
if createAttributes then
    Set att_Owner_Owner = DefineAttachment (MyDictionary, "Owner", "CaseTalk Owner", "Owner", "", "", 5)
    Set att_Owner_Department = DefineAttachment (MyDictionary, "Owner", "CaseTalk Owner", "Department", "", "", 5)
    Set att_Owner_Date = DefineAttachment (MyDictionary, "Owner", "CaseTalk Owner", "Date", "", "", 5)
    Set att_Owner_Definition = DefineAttachment (MyDictionary, "Owner", "CaseTalk Owner", "Definition", "", "", 5)
    Set att_Source_Interviewer = DefineAttachment (MyDictionary, "Source", "CaseTalk Source", "Interviewer", "", "", 5)
    Set att_Source_DomainExpert = DefineAttachment (MyDictionary, "Source", "CaseTalk Source", "DomainExpert", "", "", 5)
    Set att_Source_Department = DefineAttachment (MyDictionary, "Source", "CaseTalk Source", "Department", "", "", 5)
    Set att_Source_Date = DefineAttachment (MyDictionary, "Source", "CaseTalk Source", "Date", "", "", 5)
    Set att_Source_Locale = DefineAttachment (MyDictionary, "Source", "CaseTalk Source", "Locale", "", "", 5)
    Set att_Source_Concept = DefineAttachment (MyDictionary, "Source", "CaseTalk Source", "Concept", "", "", 5)
    Set att_Task_Status = DefineAttachment (MyDictionary, "Task", "CaseTalk Task", "Status", "", "", 5)
end if

'Create Domains

Set Domain_fysieke_id = MyDictionary.Domains.Item("fysieke_id")
if Domain_fysieke_id is Nothing then
    Set Domain_fysieke_id = MyDictionary.Domains.Add("fysieke_id", "fysieke_id")
    end if
    Domain_fysieke_id.DataType = "char"
    Domain_fysieke_id.DataLength = 52

Set Domain_gegevensgebied_id = MyDictionary.Domains.Item("gegevensgebied_id")
if Domain_gegevensgebied_id is Nothing then
    Set Domain_gegevensgebied_id = MyDictionary.Domains.Add("gegevensgebied_id", "gegevensgebied_id")
    end if
    Domain_gegevensgebied_id.DataType = "char"
    Domain_gegevensgebied_id.DataLength = 3

Set Domain_gegevensmiddel_id = MyDictionary.Domains.Item("gegevensmiddel_id")
if Domain_gegevensmiddel_id is Nothing then
    Set Domain_gegevensmiddel_id = MyDictionary.Domains.Add("gegevensmiddel_id", "gegevensmiddel_id")
    end if
    Domain_gegevensmiddel_id.DataType = "integer"

Set Domain_gegevensverzameling_id = MyDictionary.Domains.Item("gegevensverzameling_id")
if Domain_gegevensverzameling_id is Nothing then
    Set Domain_gegevensverzameling_id = MyDictionary.Domains.Add("gegevensverzameling_id", "gegevensverzameling_id")
    end if
    Domain_gegevensverzameling_id.DataType = "char"
    Domain_gegevensverzameling_id.DataLength = 4

Set Domain_naam = MyDictionary.Domains.Item("naam")
if Domain_naam is Nothing then
    Set Domain_naam = MyDictionary.Domains.Add("naam", "naam")
    end if
    Domain_naam.DataType = "char"
    Domain_naam.DataLength = 35

' Create entity for: Gegevensgebied

Set Entity_Gegevensgebied = MyModel.Entities.Item("Gegevensgebied")
if Entity_Gegevensgebied is Nothing then
    Set Entity_Gegevensgebied = MyModel.Entities.Add(250, 0)
    end if
    Entity_Gegevensgebied.TableName = "Gegevensgebied"
    Entity_Gegevensgebied.EntityName = "Gegevensgebied"
    Entity_Gegevensgebied.Note ="[Expressions]" & vbCrLf & _
        "Er is een gegevensgebied <gegevensgebied id>." & vbCrLf & _
        "Gegevensgebied <gegevensgebied id> heet <Naam Gegevensgebied>."
    if createAttributes then
        SetAttachmentValue(Entity_Gegevensgebied, att_Owner_Date, "12/22/2021 3:21:49 PM")
        SetAttachmentValue(Entity_Gegevensgebied, att_Source_Date, "12/22/2021 3:21:49 PM")
    end if
Set Attribute_Gegevensgebied_gegevensgebied_id = Entity_Gegevensgebied.Attributes.Item("gegevensgebied id")
if Attribute_Gegevensgebied_gegevensgebied_id is Nothing then
    Set Attribute_Gegevensgebied_gegevensgebied_id = Entity_Gegevensgebied.Attributes.Add("gegevensgebied id", True)
    end if
    Attribute_Gegevensgebied_gegevensgebied_id.DomainId = Domain_gegevensgebied_id.ID
    Attribute_Gegevensgebied_gegevensgebied_id.LogicalRoleName = "gegevensgebied id"
    Attribute_Gegevensgebied_gegevensgebied_id.NullOption = "NOT NULL"
Set Attribute_Gegevensgebied_Naam_Gegevensgebied = Entity_Gegevensgebied.Attributes.Item("Naam Gegevensgebied")
if Attribute_Gegevensgebied_Naam_Gegevensgebied is Nothing then
    Set Attribute_Gegevensgebied_Naam_Gegevensgebied = Entity_Gegevensgebied.Attributes.Add("Naam Gegevensgebied", False)
    end if
    Attribute_Gegevensgebied_Naam_Gegevensgebied.DomainId = Domain_naam.ID
    Attribute_Gegevensgebied_Naam_Gegevensgebied.LogicalRoleName = "Naam Gegevensgebied"


' Create entity for: Gegevensmiddel

Set Entity_Gegevensmiddel = MyModel.Entities.Item("Gegevensmiddel")
if Entity_Gegevensmiddel is Nothing then
    Set Entity_Gegevensmiddel = MyModel.Entities.Add(0, 250)
    end if
    Entity_Gegevensmiddel.TableName = "Gegevensmiddel"
    Entity_Gegevensmiddel.EntityName = "Gegevensmiddel"
    Entity_Gegevensmiddel.Note ="[Expressions]" & vbCrLf & _
        "Er is een gegevensmiddel <gegevensmiddel id>." & vbCrLf & _
        "De fysieke locatie van gegevensmiddel<gegevensmiddel id> is <Fysieke identifier>." & vbCrLf & _
        "Gegevensmiddel<gegevensmiddel id> is van het gegevensmiddeltype <Gegevensmiddeltype>."
    if createAttributes then
        SetAttachmentValue(Entity_Gegevensmiddel, att_Owner_Date, "12/22/2021 3:32:40 PM")
        SetAttachmentValue(Entity_Gegevensmiddel, att_Source_Date, "12/22/2021 3:32:40 PM")
    end if
Set Attribute_Gegevensmiddel_gegevensmiddel_id = Entity_Gegevensmiddel.Attributes.Item("gegevensmiddel id")
if Attribute_Gegevensmiddel_gegevensmiddel_id is Nothing then
    Set Attribute_Gegevensmiddel_gegevensmiddel_id = Entity_Gegevensmiddel.Attributes.Add("gegevensmiddel id", True)
    end if
    Attribute_Gegevensmiddel_gegevensmiddel_id.DomainId = Domain_gegevensmiddel_id.ID
    Attribute_Gegevensmiddel_gegevensmiddel_id.LogicalRoleName = "gegevensmiddel id"
    Attribute_Gegevensmiddel_gegevensmiddel_id.NullOption = "NOT NULL"
Set Attribute_Gegevensmiddel_Fysieke_identifier = Entity_Gegevensmiddel.Attributes.Item("Fysieke identifier")
if Attribute_Gegevensmiddel_Fysieke_identifier is Nothing then
    Set Attribute_Gegevensmiddel_Fysieke_identifier = Entity_Gegevensmiddel.Attributes.Add("Fysieke identifier", False)
    end if
    Attribute_Gegevensmiddel_Fysieke_identifier.DomainId = Domain_fysieke_id.ID
    Attribute_Gegevensmiddel_Fysieke_identifier.LogicalRoleName = "Fysieke identifier"
Set Attribute_Gegevensmiddel_Gegevensmiddeltype = Entity_Gegevensmiddel.Attributes.Item("Type gegevensmiddel/Gegevensmiddeltype")
if Attribute_Gegevensmiddel_Gegevensmiddeltype is Nothing then
    Set Attribute_Gegevensmiddel_Gegevensmiddeltype = Entity_Gegevensmiddel.Attributes.Add("Type gegevensmiddel/Gegevensmiddeltype", False)
    end if
    Attribute_Gegevensmiddel_Gegevensmiddeltype.DomainId = Domain_naam.ID
    Attribute_Gegevensmiddel_Gegevensmiddeltype.LogicalRoleName = "Type gegevensmiddel/Gegevensmiddeltype"


' Create entity for: Gegevensverzameling

Set Entity_Gegevensverzameling = MyModel.Entities.Item("Gegevensverzameling")
if Entity_Gegevensverzameling is Nothing then
    Set Entity_Gegevensverzameling = MyModel.Entities.Add(250, 250)
    end if
    Entity_Gegevensverzameling.TableName = "Gegevensverzameling"
    Entity_Gegevensverzameling.EntityName = "Gegevensverzameling"
    Entity_Gegevensverzameling.Note ="[Expressions]" & vbCrLf & _
        "Er is een gegevensverzameling <gegevensverzameling id>." & vbCrLf & _
        "Gegevensverzameling <gegevensverzameling id> hoort bij het gegevensgebied <Gegevensgebied>." & vbCrLf & _
        "Gegevensverzameling <gegevensverzameling id> heet <Naam Gegevensverzameling>."
    if createAttributes then
        SetAttachmentValue(Entity_Gegevensverzameling, att_Owner_Date, "12/22/2021 3:22:06 PM")
        SetAttachmentValue(Entity_Gegevensverzameling, att_Source_Date, "12/22/2021 3:22:06 PM")
    end if
Set Attribute_Gegevensverzameling_gegevensverzameling_id = Entity_Gegevensverzameling.Attributes.Item("gegevensverzameling id")
if Attribute_Gegevensverzameling_gegevensverzameling_id is Nothing then
    Set Attribute_Gegevensverzameling_gegevensverzameling_id = Entity_Gegevensverzameling.Attributes.Add("gegevensverzameling id", True)
    end if
    Attribute_Gegevensverzameling_gegevensverzameling_id.DomainId = Domain_gegevensverzameling_id.ID
    Attribute_Gegevensverzameling_gegevensverzameling_id.LogicalRoleName = "gegevensverzameling id"
    Attribute_Gegevensverzameling_gegevensverzameling_id.NullOption = "NOT NULL"
    ' An incoming "foreign key - column pair" should automatically inject this column: GV van GB/Gegevensgebied
Set Attribute_Gegevensverzameling_Naam_Gegevensverzameling = Entity_Gegevensverzameling.Attributes.Item("Naam Gegevensverzameling")
if Attribute_Gegevensverzameling_Naam_Gegevensverzameling is Nothing then
    Set Attribute_Gegevensverzameling_Naam_Gegevensverzameling = Entity_Gegevensverzameling.Attributes.Add("Naam Gegevensverzameling", False)
    end if
    Attribute_Gegevensverzameling_Naam_Gegevensverzameling.DomainId = Domain_naam.ID
    Attribute_Gegevensverzameling_Naam_Gegevensverzameling.LogicalRoleName = "Naam Gegevensverzameling"


' Create entity for: GV_2_GM

Set Entity_GV_2_GM = MyModel.Entities.Item("GV_2_GM")
if Entity_GV_2_GM is Nothing then
    Set Entity_GV_2_GM = MyModel.Entities.Add(0, 500)
    end if
    Entity_GV_2_GM.TableName = "GV_2_GM"
    Entity_GV_2_GM.EntityName = "GV_2_GM"
    Entity_GV_2_GM.Note ="[Expressions]" & vbCrLf & _
        "Data die behoort tot gegevensverzameling <Gegevensverzameling> is opgeslagen in gegevensmiddel<Gegevensmiddel>."
    if createAttributes then
        SetAttachmentValue(Entity_GV_2_GM, att_Owner_Date, "12/22/2021 3:34:27 PM")
        SetAttachmentValue(Entity_GV_2_GM, att_Source_Date, "12/22/2021 3:34:27 PM")
    end if
    ' An incoming "foreign key - column pair" should automatically inject this column: Gegevensverzameling
    ' An incoming "foreign key - column pair" should automatically inject this column: Gegevensmiddel


' Creating Relationships

' Set relationShip = MyModel.Relationships.AddWithRoleName(
'       Parent Entity, Child Entity, RelationshipType,  
'       "PKname1,RoleName1;PKName2,RoleName2;.." )

Set FK_Gegevensgebied_in_Gegevensverzameling = MyModel.Relationships.Item("FK_Gegevensgebied_in_Gegevensverzameling")
if FK_Gegevensgebied_in_Gegevensverzameling is nothing then
Set FK_Gegevensgebied_in_Gegevensverzameling = MyModel.Relationships.AddWithRoleName( _
      Entity_Gegevensgebied.EntityName, Entity_Gegevensverzameling.EntityName, 1, _
      "gegevensgebied id, GV van GB/Gegevensgebied")
    end if
    If FK_Gegevensgebied_in_Gegevensverzameling.ID <> 0 Then ' succesful
      FK_Gegevensgebied_in_Gegevensverzameling.Name = "FK_Gegevensgebied_in_Gegevensverzameling"
      FK_Gegevensgebied_in_Gegevensverzameling.BusinessName = "Gegevensgebied in Gegevensverzameling"
      FK_Gegevensgebied_in_Gegevensverzameling.CardinalityNo = 0
      For Each fkpair In FK_Gegevensgebied_in_Gegevensverzameling.FkColumnPairs
        fkpair.ChildAttribute.RoleName = fkpair.ChildAttribute.LogicalRoleName
      Next
    End If

Set FK_Gegevensverzameling_in_GV_2_GM = MyModel.Relationships.Item("FK_Gegevensverzameling_in_GV_2_GM")
if FK_Gegevensverzameling_in_GV_2_GM is nothing then
Set FK_Gegevensverzameling_in_GV_2_GM = MyModel.Relationships.AddWithRoleName( _
      Entity_Gegevensverzameling.EntityName, Entity_GV_2_GM.EntityName, 0, _
      "gegevensverzameling id, Gegevensverzameling")
    end if
    If FK_Gegevensverzameling_in_GV_2_GM.ID <> 0 Then ' succesful
      FK_Gegevensverzameling_in_GV_2_GM.Name = "FK_Gegevensverzameling_in_GV_2_GM"
      FK_Gegevensverzameling_in_GV_2_GM.BusinessName = "Gegevensverzameling in GV_2_GM"
      FK_Gegevensverzameling_in_GV_2_GM.CardinalityNo = 0
      For Each fkpair In FK_Gegevensverzameling_in_GV_2_GM.FkColumnPairs
        fkpair.ChildAttribute.RoleName = fkpair.ChildAttribute.LogicalRoleName
      Next
    End If

Set FK_Gegevensmiddel_in_GV_2_GM = MyModel.Relationships.Item("FK_Gegevensmiddel_in_GV_2_GM")
if FK_Gegevensmiddel_in_GV_2_GM is nothing then
Set FK_Gegevensmiddel_in_GV_2_GM = MyModel.Relationships.AddWithRoleName( _
      Entity_Gegevensmiddel.EntityName, Entity_GV_2_GM.EntityName, 0, _
      "gegevensmiddel id, Gegevensmiddel")
    end if
    If FK_Gegevensmiddel_in_GV_2_GM.ID <> 0 Then ' succesful
      FK_Gegevensmiddel_in_GV_2_GM.Name = "FK_Gegevensmiddel_in_GV_2_GM"
      FK_Gegevensmiddel_in_GV_2_GM.BusinessName = "Gegevensmiddel in GV_2_GM"
      FK_Gegevensmiddel_in_GV_2_GM.CardinalityNo = 0
      For Each fkpair In FK_Gegevensmiddel_in_GV_2_GM.FkColumnPairs
        fkpair.ChildAttribute.RoleName = fkpair.ChildAttribute.LogicalRoleName
      Next
    End If

' Force ER/Studio to add PKColumns which may got lost due to FK creation

Set Attribute_Gegevensgebied_gegevensgebied_id = Entity_Gegevensgebied.Attributes.Item("gegevensgebied id")
if Attribute_Gegevensgebied_gegevensgebied_id is Nothing then
    Set Attribute_Gegevensgebied_gegevensgebied_id = Entity_Gegevensgebied.Attributes.Add("gegevensgebied id", True)
    end if
    Attribute_Gegevensgebied_gegevensgebied_id.DomainId = Domain_gegevensgebied_id.ID
    Attribute_Gegevensgebied_gegevensgebied_id.LogicalRoleName = "gegevensgebied id"
    Attribute_Gegevensgebied_gegevensgebied_id.NullOption = "NOT NULL"

Set Attribute_Gegevensmiddel_gegevensmiddel_id = Entity_Gegevensmiddel.Attributes.Item("gegevensmiddel id")
if Attribute_Gegevensmiddel_gegevensmiddel_id is Nothing then
    Set Attribute_Gegevensmiddel_gegevensmiddel_id = Entity_Gegevensmiddel.Attributes.Add("gegevensmiddel id", True)
    end if
    Attribute_Gegevensmiddel_gegevensmiddel_id.DomainId = Domain_gegevensmiddel_id.ID
    Attribute_Gegevensmiddel_gegevensmiddel_id.LogicalRoleName = "gegevensmiddel id"
    Attribute_Gegevensmiddel_gegevensmiddel_id.NullOption = "NOT NULL"

Set Attribute_Gegevensverzameling_gegevensverzameling_id = Entity_Gegevensverzameling.Attributes.Item("gegevensverzameling id")
if Attribute_Gegevensverzameling_gegevensverzameling_id is Nothing then
    Set Attribute_Gegevensverzameling_gegevensverzameling_id = Entity_Gegevensverzameling.Attributes.Add("gegevensverzameling id", True)
    end if
    Attribute_Gegevensverzameling_gegevensverzameling_id.DomainId = Domain_gegevensverzameling_id.ID
    Attribute_Gegevensverzameling_gegevensverzameling_id.LogicalRoleName = "gegevensverzameling id"
    Attribute_Gegevensverzameling_gegevensverzameling_id.NullOption = "NOT NULL"

    ' An incoming "foreign key - column pair" should automatically inject this column: Gegevensverzameling
    ' An incoming "foreign key - column pair" should automatically inject this column: Gegevensmiddel

' Creating Alternate Keys

Set AK1_Gegevensgebied = Entity_Gegevensgebied.Indexes.Add("Naam Gegevensgebied", "AK1_Gegevensgebied")
Set AK1_Gegevensmiddel = Entity_Gegevensmiddel.Indexes.Add("Fysieke identifier", "AK1_Gegevensmiddel")
Set AK1_Gegevensverzameling = Entity_Gegevensverzameling.Indexes.Add("Naam Gegevensverzameling", "AK1_Gegevensverzameling")

DiagramManager.EnableScreenUpdateEx(True, True) ' Turn screen updates back on
End Sub

Function DefineAttachment (ByRef aDictionary, aType, aTypeDescription, aKey, aKeyDescription, aDefaultValue, aDataType)
Set attType = aDictionary.AttachmentTypes(aType)
    If attType Is Nothing Then
    Set attType = aDictionary.AttachmentTypes.Add(aType, aTypeDescription)
    End If
	Set attDef = attType.Attachments.Item(aKey)
	If attDef Is Nothing Then
		Set attDef = attType.Attachments.Add(aKey, aKeyDescription, aDefaultValue, aDataType)
	End If
	Set DefineAttachment = attDef
End Function

Sub SetAttachmentValue (ByRef aEntity, ByRef aAttDef, aValue)
	If not IsEmpty(aAttDef) Then
		Set attValue = aEntity.BoundAttachments.Item(aAttDef.ID)
		If attValue Is Nothing Then
			Set attValue = aEntity.BoundAttachments.Add(aAttDef.ID)
		End If
		attValue.valueOverride = aValue
	End If
End Sub

